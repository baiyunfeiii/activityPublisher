package cn.cooode.activityTools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityToolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityToolsApplication.class, args);
	}

}

