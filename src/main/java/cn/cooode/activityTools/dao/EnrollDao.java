package cn.cooode.activityTools.dao;

import cn.cooode.activityTools.entity.Enroll;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnrollDao extends JpaRepository<Enroll, Long> {

    long countAllByActivity_IdAndUser_Id(Long activityId, Long userId);

    Page<Enroll> findByActivity_Id(Long activityId, Pageable pageable);

    List<Enroll> findByUser_Id(Long userId);


}
