package cn.cooode.activityTools.dao;

import cn.cooode.activityTools.entity.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface ActivityDao extends JpaRepository<Activity, Long> {

    Page<Activity> findByCategory_Id(Long categoryId, Pageable pageable);
    List<Activity> findAllByCategory_Id(Long categoryId);

    Page<Activity> findByCategory_IdAndActivityCheck_Pass(Long category_id, Boolean activityCheck_pass, Pageable pageable);

    Long countByCategory_Id(Long categoryId);

    Page<Activity> findByActivityCheck_Pass(boolean status, Pageable pageable);

    Page<Activity> findByIdIn(List<Long> ids, Pageable pageable);

    Page<Activity> findByIdInAndStartTimeIsAfter(Collection<Long> id, Date startTime, Pageable pageable);

    Page<Activity> findByAuthor_Id(Long authorId, Pageable pageable);

}
