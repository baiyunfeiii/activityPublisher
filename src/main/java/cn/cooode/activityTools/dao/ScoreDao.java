package cn.cooode.activityTools.dao;

import cn.cooode.activityTools.entity.Score;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoreDao extends JpaRepository<Score, Long> {
    Page<Score> findByActivity_Id(Long activityId, Pageable pageable);

    Page<Score> findByActivity_IdAndUser_NameIsLikeAndUser_PhoneIsLikeAndUser_EmailIsLike(Long activityId, String name, String phone, String email, Pageable pageable);
}
