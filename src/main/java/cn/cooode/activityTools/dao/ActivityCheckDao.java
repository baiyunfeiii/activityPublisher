package cn.cooode.activityTools.dao;


import cn.cooode.activityTools.entity.ActivityCheck;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityCheckDao extends JpaRepository<ActivityCheck, Long> {

    @Query(value = "from ActivityCheck ac order by ac.processed asc, ac.activity.publishTime desc")
    Page<ActivityCheck> find(Pageable pageable);

}
