package cn.cooode.activityTools.dao;

import cn.cooode.activityTools.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryDao extends JpaRepository<Category, Long> {
    List<Category> findByStateOrderByName(Byte state);

    Category findByName(String name);
}
