package cn.cooode.activityTools.service;

import cn.cooode.activityTools.entity.Score;
import org.springframework.data.domain.Page;

/**
 * Created by BaiYunfei on 2017/1/8.
 */
public interface ScoreService {

    Score get(Long id);

    Score save(Score score);

    void update(Score score);

    void delete(Long id);

    Page<Score> getPager(Long activityId, int page, int size);

    Page<Score> getPager(Long activityId, String query, int page, int size);

}
