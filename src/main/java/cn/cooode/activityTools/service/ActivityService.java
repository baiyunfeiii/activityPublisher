package cn.cooode.activityTools.service;

import cn.cooode.activityTools.entity.Activity;
import cn.cooode.activityTools.entity.Pager;
import org.springframework.data.domain.Page;

/**
 * Created by BaiYunfei on 2017/1/4.
 */
public interface ActivityService {

    Page<Activity> getPager(int page, int size);

    Page<Activity> getPagerByCategory(Long categoryId, int page, int size);

    Activity get(Long id);

    Page<Activity> getEnrolled(Long userId, int page, int size);

    Page<Activity> getPassed(Long userId, int page, int size);

    Page<Activity> getPublished(Long userId, int page, int size);

    Activity save(Activity activity);

}
