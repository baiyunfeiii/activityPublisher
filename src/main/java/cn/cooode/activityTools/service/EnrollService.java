package cn.cooode.activityTools.service;

import cn.cooode.activityTools.entity.Enroll;
import cn.cooode.activityTools.entity.Pager;
import org.springframework.data.domain.Page;

/**
 * Created by BaiYunfei on 2017/1/5.
 */
public interface EnrollService {

    public Enroll save(Enroll enroll);

    public boolean hasEnrolled(Long activityId,Long userId);

    public Page<Enroll> getEnrollTable(Long activityId, int page, int size);

}
