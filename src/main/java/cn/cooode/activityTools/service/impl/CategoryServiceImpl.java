package cn.cooode.activityTools.service.impl;

import cn.cooode.activityTools.constants.EntityConstants;
import cn.cooode.activityTools.dao.ActivityDao;
import cn.cooode.activityTools.dao.CategoryDao;
import cn.cooode.activityTools.entity.Activity;
import cn.cooode.activityTools.entity.Category;
import cn.cooode.activityTools.entity.Pager;
import cn.cooode.activityTools.service.CategoryService;
import cn.cooode.activityTools.util.SystemContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by BaiYunfei on 2017/1/5.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryDao categoryDao;
    @Autowired
    ActivityDao activityDao;

    public Category save(Category category) {
        return categoryDao.save(category);
    }

    public void update(Category category) {
        categoryDao.save(category);
    }

    public Category get(Long id) {
        return categoryDao.findById(id).get();
    }

    public List<Category> list() {
        return categoryDao.findByStateOrderByName(EntityConstants.CATEGORY_STATE_NORMAL);
    }

    public Long activityCount(Long categoryId) {
        return activityDao.countByCategory_Id(categoryId);
    }

    public void delete(Long categoryId){
        Category category = categoryDao.findById(categoryId).get();
        category.setState(EntityConstants.CATEGORY_STATE_DELETED);
        categoryDao.save(category);

        //获取类别“未分类”
        Category defaultCategory = categoryDao.findByName(EntityConstants.CATEGORY_DEFAULT);

        //把原分类中的文章移动到“未分类”中
        List<Activity> ActivityList = activityDao.findAllByCategory_Id(category.getId());
        for(Activity Activity: ActivityList){
            Activity.setCategory(defaultCategory);
            activityDao.save(Activity);
        }
        //修改“未分类”的数量
        categoryDao.save(defaultCategory);
    }

}
