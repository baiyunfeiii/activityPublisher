package cn.cooode.activityTools.service.impl;

import cn.cooode.activityTools.dao.ActivityDao;
import cn.cooode.activityTools.dao.EnrollDao;
import cn.cooode.activityTools.entity.Activity;
import cn.cooode.activityTools.entity.Enroll;
import cn.cooode.activityTools.entity.Pager;
import cn.cooode.activityTools.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by BaiYunfei on 2017/1/5.
 */
@Service
@Transactional
public class EnrollServiceImpl implements EnrollService {

    @Autowired
    EnrollDao enrollDao;
    @Autowired
    ActivityDao activityDao;

    public Enroll save(Enroll enroll) {
        return enrollDao.save(enroll);
    }

    public boolean hasEnrolled(Long activityId, Long userId) {
        return enrollDao.countAllByActivity_IdAndUser_Id(activityId, userId) > 0;
    }

    public Page<Enroll> getEnrollTable(Long activityId, int page, int size) {
        return enrollDao.findByActivity_Id(activityId, PageRequest.of(page, size));
    }
}
