package cn.cooode.activityTools.service.impl;

import cn.cooode.activityTools.dao.ActivityCheckDao;
import cn.cooode.activityTools.entity.ActivityCheck;
import cn.cooode.activityTools.entity.Pager;
import cn.cooode.activityTools.service.ActivityCheckSerivce;
import cn.cooode.activityTools.service.UserService;
import cn.cooode.activityTools.util.SystemContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by BaiYunfei on 2017/1/5.
 */
@Service
@Transactional
public class ActivityCheckServiceImpl implements ActivityCheckSerivce {

    @Autowired
    ActivityCheckDao activityCheckDao;
    @Autowired
    UserService userService;

    public ActivityCheck save(ActivityCheck activityCheck) {
        return activityCheckDao.save(activityCheck);
    }

    public Page<ActivityCheck> getPager(int page, int size) {
        return activityCheckDao.find(PageRequest.of(page, size));
    }

    public ActivityCheck get(Long id) {
        return activityCheckDao.findById(id).get();
    }

    public ActivityCheck handle(Long id,boolean process,Long userId){
        ActivityCheck activityCheck = get(id);
        activityCheck.setPass(process);
        activityCheck.setCheckTime(new Date());
        activityCheck.setChecker(userService.get(userId));
        activityCheck.setProcessed(true);
        activityCheckDao.save(activityCheck);
        return activityCheck;
    }

    public void update(ActivityCheck activityCheck) {
        activityCheckDao.save(activityCheck);
    }
}
