package cn.cooode.activityTools.service.impl;

import cn.cooode.activityTools.dao.UserDao;
import cn.cooode.activityTools.entity.User;
import cn.cooode.activityTools.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by BaiYunfei on 2017/1/3.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public User get(Long id) {
        return userDao.findById(id).get();
    }

    public User save(User user) {
        return userDao.save(user);
    }

    public User update(User user) {
        userDao.save(user);
        return user;
    }

    public void delete(User user) {
        userDao.delete(user);
    }

    public User login(String username, String password) {
        User user = userDao.findByUsername(username);
        //用户不存在则登录失败，返回null
        if (user == null) {
            return null;
        }
        //比较密码
        if(user.getPassword().equals(password)){
            return user;
        }
        user.setLastLoginTime(new Date());
        //密码错误
        return null;
    }


    public boolean isEmpty(String username) {
        return !existUsername(username);
    }

    public boolean existUsername(String username) {
        return userDao.existsByUsername(username);
    }
}
