package cn.cooode.activityTools.service.impl;

import cn.cooode.activityTools.dao.ScoreDao;
import cn.cooode.activityTools.entity.Pager;
import cn.cooode.activityTools.entity.Score;
import cn.cooode.activityTools.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by BaiYunfei on 2017/1/8.
 */
@Service
@Transactional
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    ScoreDao scoreDao;

    public Score get(Long id) {
        return scoreDao.findById(id).get();
    }

    public Score save(Score score) {
        return scoreDao.save(score);
    }

    public void update(Score score) {
        scoreDao.save(score);
    }

    public void delete(Long id) {
        scoreDao.delete(scoreDao.findById(id).get());
    }

    public Page<Score> getPager(Long activityId, int page, int size) {
        return scoreDao.findByActivity_Id(activityId, PageRequest.of(page, size));
    }

    public Page<Score> getPager(Long activityId, String query, int page, int size) {
        return scoreDao.findByActivity_IdAndUser_NameIsLikeAndUser_PhoneIsLikeAndUser_EmailIsLike(activityId, query, query, query, PageRequest.of(page, size));
    }
}
